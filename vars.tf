# Пул Облачной платформы
variable "region" {
  default = "ru-3"
}

# Зона
variable "az_zone" {
  default = "ru-3a"
}

# CIDR подсети
variable "subnet_cidr" {
  default = "10.10.0.0/24"
}

# Имя ssh ключа
variable "ssh_key" {
  default = "Aria"
}