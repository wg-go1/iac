terraform {
  backend "http" {}
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "1.51.1"
    }
    selectel = {
      source = "selectel/selectel"
      version = "3.9.1"
    }
  }
}

# Extnet
data "openstack_networking_network_v2" "external_net" {
  name = "external-network"
}

# Привавтая сеть core
resource "openstack_networking_network_v2" "network_core" {
  name = "network_core"
}

# Привавтая сеть web
resource "openstack_networking_network_v2" "network_web" {
  name = "network_web"
}


# Приватная подсеть core
resource "openstack_networking_subnet_v2" "subnet_core" {
  network_id = openstack_networking_network_v2.network_core.id
  name       = "subnet_core"
  cidr       = var.subnet_cidr
}

# Приватная подсеть web
resource "openstack_networking_subnet_v2" "subnet_web" {
  network_id = openstack_networking_network_v2.network_web.id
  name       = "subnet_web"
  cidr       = var.subnet_cidr
}

# Создание роутера core c внешним ip 
resource "openstack_networking_router_v2" "router_core" {
  name                = "router_core"
  external_network_id = data.openstack_networking_network_v2.external_net.id
}

# Создание роутера web c внешним ip 
resource "openstack_networking_router_v2" "router_web" {
  name                = "router_web"
  external_network_id = data.openstack_networking_network_v2.external_net.id
}

# Подключение роутера core к приватной подсети core
resource "openstack_networking_router_interface_v2" "router_core_connect" {
  router_id = openstack_networking_router_v2.router_core.id
  subnet_id = openstack_networking_subnet_v2.subnet_core.id
}

# Подключение роутера web к приватной подсети web
resource "openstack_networking_router_interface_v2" "router_web_connect" {
  router_id = openstack_networking_router_v2.router_web.id
  subnet_id = openstack_networking_subnet_v2.subnet_web.id
}





# Поиск ID образа (из которого будет создан сервер) по его имени
data "openstack_images_image_v2" "ubuntu_image" {
  most_recent = true
  visibility  = "public"
  name        = "Ubuntu 20.04 LTS 64-bit"
}

# Создание конфигурации сервера с 2 ГБ RAM, 1 vCPU, 16 ГБ Disk
data "openstack_compute_flavor_v2" "flavor_server" {
    name = "BL1.1-2048-16"
}

data "openstack_compute_keypair_v2" "ssh_key" {
  name       = var.ssh_key
}

# Создание core сервера
 resource "openstack_compute_instance_v2" "core_server" {
  name              = "server_core"
  flavor_id         = data.openstack_compute_flavor_v2.flavor_server.id
  image_id          = data.openstack_images_image_v2.ubuntu_image.id
  key_pair          = data.openstack_compute_keypair_v2.ssh_key.id
  availability_zone = var.az_zone

  network {
    uuid = openstack_networking_network_v2.network_core.id
  }
 }

 # Создание web сервера
 resource "openstack_compute_instance_v2" "web_server" {
  name              = "server_web"
  flavor_id         = data.openstack_compute_flavor_v2.flavor_server.id
  image_id          = data.openstack_images_image_v2.ubuntu_image.id
  key_pair          = data.openstack_compute_keypair_v2.ssh_key.id
  availability_zone = var.az_zone

  network {
    uuid = openstack_networking_network_v2.network_web.id
  }
 }

# Создание публичного core IP-адреса
resource "openstack_networking_floatingip_v2" "fip_core" {
  pool = "external-network"
}

# Создание публичного web IP-адреса
resource "openstack_networking_floatingip_v2" "fip_web" {
  pool = "external-network"
}

# Привязка публичного core IP-адреса к core серверу
resource "openstack_compute_floatingip_associate_v2" "bind_fip_core" {
  floating_ip = openstack_networking_floatingip_v2.fip_core.address
  instance_id = openstack_compute_instance_v2.core_server.id
}

# Привязка публичного web IP-адреса к web серверу
resource "openstack_compute_floatingip_associate_v2" "bind_fip_web" {
  floating_ip = openstack_networking_floatingip_v2.fip_web.address
  instance_id = openstack_compute_instance_v2.web_server.id
}



output "core_ip" {
  value = openstack_networking_floatingip_v2.fip_core.address
}

output "web_ip" {
  value = openstack_networking_floatingip_v2.fip_web.address
}
